import * as React from 'react'

import { 
  expect, 
  test,
  describe
} from '@jest/globals'

import * as Enzyme from 'enzyme'
import ReactSeventeenAdapter from '@wojtekmaj/enzyme-adapter-react-17'

import Dummy from '../src/components/Dummy'

Enzyme.configure({ adapter: new ReactSeventeenAdapter() })

describe('Check languages translations', () => {
  const component = Enzyme.shallow(<Dummy />)
  const spanishRender = '<div><div>Bienvenido a React 1</div><div>Bienvenido a React 2</div></div>'
  const englishRender = '<div><div>Welcome to React 1</div><div>Welcome to React 2</div></div>'

  test('"default"', () => {
    expect(component.html()).toEqual(spanishRender)
  })

  test('set "es"', () => {
    component.setProps({ lng: 'es' })
  
    expect(component.html()).toEqual(spanishRender)
  })

  test('set "en"', () => {
    component.setProps({ lng: 'en' })
  
    expect(component.html()).toEqual(englishRender)
  })
})
