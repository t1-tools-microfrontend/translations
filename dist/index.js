"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var i18next_1 = __importDefault(require("i18next"));
var react_i18next_1 = require("react-i18next");
var TranslationsInit = (function () {
    function TranslationsInit(lng, resources) {
        this.lng = lng;
        this.resources = resources;
        return this.initializeI18();
    }
    TranslationsInit.prototype.initializeI18 = function () {
        i18next_1["default"]
            .use(react_i18next_1.initReactI18next)
            .init({
            lng: this.lng,
            resources: this.resources,
            ns: ['translation'],
            defaultNS: 'translation'
        });
        return i18next_1["default"];
    };
    return TranslationsInit;
}());
exports["default"] = TranslationsInit;
//# sourceMappingURL=index.js.map