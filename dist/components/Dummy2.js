"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var React = __importStar(require("react"));
var react_i18next_1 = require("react-i18next");
var __1 = __importDefault(require("../"));
var Dummy2 = function (_a) {
    var lng = _a.lng;
    var newI18Instance = new __1["default"]('es', {
        es: {
            translation: {
                'Welcome to React': 'Bienvenido a React 2'
            }
        },
        en: {
            translation: {
                'Welcome to React': 'Welcome to React 2'
            }
        }
    });
    var _b = react_i18next_1.useTranslation(), t = _b.t, i18n = _b.i18n;
    i18n.changeLanguage(lng);
    return (React.createElement("div", null, newI18Instance.t('Welcome to React')));
};
exports["default"] = Dummy2;
//# sourceMappingURL=Dummy2.js.map