import i18next from 'i18next'

import {
  initReactI18next
} from 'react-i18next'

interface IResources {
  [key: string]: {
    [key: string]: any
  }
}

class TranslationsInit {
  lng: string
  
  resources: IResources

  constructor(lng: string, resources: any) {
    this.lng = lng
    this.resources = resources

    return this.initializeI18()
  }

  initializeI18(): any {
    i18next
      .use(initReactI18next)
      .init({
        lng: this.lng,
        resources: this.resources,
        ns: ['translation'],
        defaultNS: 'translation'    
      })
    
    return i18next 
  }
}

export default TranslationsInit
