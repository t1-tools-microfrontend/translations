import * as React from 'react'

import {
  useTranslation
} from 'react-i18next'

import TranslationsInit from '../'

interface Props {
  lng?: string
}

const Dummy2: React.FC<Props> = ({ lng }) => {
  new TranslationsInit('es', {
    es: {
      translation: {
        'Welcome to React': 'Bienvenido a React 2'
      }
    },
    en: {
      translation: {
        'Welcome to React': 'Welcome to React 2'
      }
    }
  })

  const {
    t,
    i18n
  } = useTranslation()

  i18n.changeLanguage(lng)
  
  return (
    <div>
      {t('Welcome to React')}
    </div>
  )
}

export default Dummy2
