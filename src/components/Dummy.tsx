import * as React from 'react'

import Dummy1 from './Dummy1'
import Dummy2 from './Dummy2'

interface Props {
  lng?: string
}

const Dummy: React.FC<Props> = ({ lng }) => {
  return (
    <div>
      <Dummy1
        lng={lng}
      />
      <Dummy2
        lng={lng}
      />
    </div>
  )
}

export default Dummy
