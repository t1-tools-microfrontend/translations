import * as React from 'react'

import {
  useTranslation
} from 'react-i18next'

import TranslationsInit from '../'

interface Props {
  lng?: string
}

const Dummy1: React.FC<Props> = ({ lng }) => {
  new TranslationsInit('es', {
    es: {
      translation: {
        'Welcome to React': 'Bienvenido a React 1'
      }
    },
    en: {
      translation: {
        'Welcome to React': 'Welcome to React 1'
      }
    }
  })

  const {
    t,
    i18n
  } = useTranslation()

  i18n.changeLanguage(lng)
  
  return (
    <div>
      {t('Welcome to React')}
    </div>
  )
}

export default Dummy1
